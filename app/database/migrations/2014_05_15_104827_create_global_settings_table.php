<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create ( 'global_settings', function ($table) {
			$table->increments('id');
			$table->integer ( 'permissable_loan_period' )->default('2');
			$table->integer ( 'global_book_allowance_under1yr' )->default('4');
			$table->integer ( 'global_book_allowance_over1yr' )->default('6');
			$table->integer ('loan_rate')->default('5');
			$table->integer ('show_most_borrowed')->default('5');
			$table->integer ('show_highest_rated')->default('5');
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
