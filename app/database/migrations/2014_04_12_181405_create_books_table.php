<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration {
	
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'books', function ($table) {
			$table->increments ( 'id' );
			$table->string ( 'title' );
			$table->string ( 'isbn' );
			//$table->string ( 'isbn' )->unique ();
			$table->date ( 'publication_date' );
			$table->integer ( 'author_id' );
			$table->integer ( 'genre_id' );
			$table->double ( 'average_review_score' )->default(0.00);
			$table->boolean('reserved')->default(false);
			$table->boolean('available')->default(true);
			$table->string('status', 255)->nullable();
			$table->string('comment', 255)->nullable();
			$table->timestamps ();
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('books');
	}
}
