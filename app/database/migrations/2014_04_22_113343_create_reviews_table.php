<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create ( 'reviews', function ($table) {
			$table->increments ( 'id' );
			$table->integer ( 'book_id' );
			$table->integer ( 'user_id' );
			$table->integer ( 'rating' );
			$table->string ( 'comment' );
			$table->timestamps ();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop ( 'reviews' );
	}

}
