<?php

// Default home routes for various sections based on authority

Route::get('/', 'HomeController@showWelcome');
Route::get('/administrator', 'AdminController@home');
Route::get('/librarian', 'LibrarianController@home');
Route::get('/member', 'HomeController@showWelcome');

// Routes for global settings
Route::get('/globalSettings', 'GlobalSettingController@getSettings');
Route::post('/refreshGlobalSettings', 'GlobalSettingController@changeSettings');

// Administrator report route
Route::get ('/adminReport', 'ReportController@showMostFined');

// Routes for all of the actions of users e. login, signin i.e authorization and authentication
Route::controller('users', 'UsersController');
Route::post('signin', 'UsersController@signin');

// RESTful route for a book resource
Route::resource('book', 'BookController');

// RESTful route for an author resource
Route::resource('author', 'AuthorController');

// RESTful route for a genre resource
Route::resource('genre', 'GenreController');

// route for listing members
Route::get ('/listMembers', 'MemberController@index');

// Routes to handle book searches
Route::get('bookSearch', 'BookSearchController@showSearchBooks');
Route::post('bookSearch', 'BookSearchController@bookSearchResults');

// RESTful route to handle approval of registered members
Route::resource('approval', 'approveMemberController');
Route::get('approval', 'approveMemberController@index');
Route::post('approval', 'approveMemberController@postUpdate');

// RESTful route to handle member reviews of books
Route::resource('review', 'ReviewController');
Route::get('/createReview/{item_id}', 'ReviewController@create');
Route::post('/createReview', 'ReviewController@reviewConfrim');

// Routes to handle the lending of items - books
Route::get ('/checkoutBook', 'CheckoutController@showBookCheckout');
Route::post('/doCheckoutGetMember', 'CheckoutController@doCheckoutGetMember');
Route::post ('/doCheckoutBook', 'CheckoutController@doBookCheckout');

// Routes to handle the return of items - books
Route::get ('/checkinBookByMemberId', 'CheckinController@showBookCheckinByMemberId');
Route::get ('/checkinBookByBookId', 'CheckinController@showBookCheckinByBookId');
Route::post('/doCheckinGetMember', 'CheckinController@doCheckinGetMember');
Route::post('/doCheckinGetBook', 'CheckinController@doCheckinGetBook');
Route::post ('/doCheckinBook', 'CheckinController@doBookCheckin');

// Routes to handle fines
Route::get('/membersWithFines', 'FinesController@showMembersWithFines');
Route::post('/updateMembersWithFines', 'FinesController@updateMembersWithFines');

