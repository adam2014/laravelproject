<?php

class BookSearchController extends BaseController {
	
	public function bookSearchResults() {
		
		$inputs = Input::all ();
		$searchTerm = $inputs ['searchTerm'];
		$criteria = $inputs ['criteria'];
		
		if ($criteria === 'author') {
			
			// Beautiful piece of code, isn't it?
			// We need power here so, we use the whereHas method to put "where"
			// condition on the has queries to look for books with the author
			// of the given name. Notice the clever use of the use method to
			// pass the searchTerm into the function.
			$books = Book::with ( 'author', 'genre' )->whereHas ( 'author', function ($query) use($searchTerm) {
				$query->where ( 'name', '=', $searchTerm );
			} )->paginate(3);
		} 
		else {
			// eager load to reduce the number of queries
			$books = Book::with ( 'author', 'genre' )->where ( $criteria, $searchTerm )->paginate(3);
		}
		
		$message = 'RESULTS OF BOOK SEARCH';
		
		return View::make ( 'book.searchResults' )->with ( 'books', $books )->with ( 'message', $message );
	}
	
	public function showSearchBooks() {
		$book = new Book ();
		
		$criteria = array (
				'title' => 'title',
				'author' => 'author',
				'id' => 'id',
				'isbn' => 'isbn',
				'publication_date' => 'publication date' 
		);
		
		return View::make ( 'book/search' )->with ( 'book', $book )->with ( 'criteria', $criteria );
	}
}