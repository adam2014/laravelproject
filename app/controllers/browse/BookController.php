<?php

class BookController extends \BaseController {
	
	// add a filter for authorization only allowing index and show resource
	function __construct() {
		 $this->beforeFilter ( 'administrator', array ('except' => array ('index', 'show', 'search')) );
		 
		 // protecting any post actions from csrf attacks
		 $this->beforeFilter('csrf', array('on'=>'post'));
	}
	
	// Display a listing of the books.
	// @return Response
	public function index() {
		
		$totalBookNumber = count(Book::all());
		// use pagination as list could be long
		$books = Book::with('author', 'genre')->orderBy('title')->paginate(10);
		
		return View::make ( 'book.index' )->with ( 'books', $books )-> with('totalBookNumber', $totalBookNumber);
	}
	
	public function search() {
		
		$type = Auth::user()->type;
	
		if ($type !== "member" || $type !== "librarian") {
			return Redirect::to('users/login');
		}
		else {
			$book = new Book ();
			$inputs = Input::all ();
			$title= $inputs ['title'];
			// eager load too
			$books = Book::with('author', 'genre')->where('title', $title)->paginate(10)->get();
			$message = 'RESULTS OF BOOK SEARCH';
			return View::make ( 'book.index' )->with ( 'book', $book )->with ( 'books', $books )->with ( 'message', $message );
		}
	}
	
	// Show the form for creating a new book.
	// @return Response
	public function create() {
		$book = new Book ();
		$authors = Author::all ();
		$genres = Genre::all ();
		return View::make ( 'book.create' )->with ( 'book', $book )->with ( 'authors', $authors )->with ( 'genres', $genres );
	}
	
	// Store a newly created book in storage.
	// @return Response
	public function store() {
		$book = new Book ();
		
		$inputs = Input::all ();
		$book->title = $inputs ['title'];
		$book->isbn = $inputs ['isbn'];
		$book->publication_date = $inputs ['publication_date'];
		$book->author_id = $inputs ['author_id'];
		$book->genre_id = $inputs ['genre_id'];
		$book->save ();
		return Redirect::route ( 'book.index' );
	}
	
	// Display the specified book.
	// @param int $id
	// @return Response
	public function show($id) {
		$book = Book::find ( $id );
		
		$author = $book->author;
		$books = $author->books;
		$message = Session::get ( 'message', '' );
		
		return View::make ( 'book.show' )->with ( 'book', $book )->with ( 'author', $author )->with ( 'books', $books )->with ( 'message', $message );
	}
	
	// Show the form for editing the specified resource.
	// @param int $id
	// @return Response
	public function edit($id) {
		$book = Book::find ( $id );
		return View::make ( 'book.edit' )->with ( 'book', $book );
	}
	
	// Update the specified resource in storage.
	// @param int $id
	// @return Response
	public function update($id) {
		$book = Book::find ( $id );
		$inputs = Input::all ();
		
		$book->title = $inputs ['title'];
		$book->isbn = $inputs ['isbn'];
		$book->publication_date = $inputs ['publication_date'];
		$book->author_id = $inputs ['author_id'];
		$book->genre_id = $inputs ['genre_id'];
		$book->update ();
		
		return Redirect::route ( 'book.show', array (
				$id 
		) )->with ( 'message', 'Book updated.' );
	}
	
	// Remove the specified resource from storage.
	// @param int $id
	// @return Response
	public function destroy($id) {
		$book = Book::find($id);
		$book->delete();
	}
}
