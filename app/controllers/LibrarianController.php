<?php

// This is the Librarian controller
class LibrarianController extends BaseController {

	function __construct() {
	
		// authenticate all actions
		$this->beforeFilter ( 'librarian', array (
				'except' => array ()
		) );
	
		// protecting any post actions from csrf attacks
		$this->beforeFilter ( 'csrf', array (
				'on' => 'post'
		) );
	}
	
	// Default Home Controller
	public function home() {
		return View::make('librarian/home');
	}

}