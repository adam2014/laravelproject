<?php

// we are going to use Carbon dates so we include it
// this is resident in the vendor folder
use Carbon\Carbon;

// controller responsible for handling the lending
// or checking out of book items
class CheckinController extends BaseController {
	public $restful = true;
	
	
	function __construct() {
		
		// authenticate all actions based on a custom filter
		$this->beforeFilter ( 'librarian', array (
				'except' => array () 
		) );
		
		// protecting any post actions from csrf attacks
		$this->beforeFilter ( 'csrf', array (
				'on' => 'post' 
		) );
	}
	
	// post action to get a member
	public function doCheckinGetMember() {
		
		// get the form inputs
		$inputs = Input::all ();
		$memberId = $inputs ['memberId'];
		
		// find the particular member
		$user = User::find ( $memberId );
		
		// make sure they are present and are members
		if (count ( $user ) && $user->hasRole ( 'member' )) {
			$booksAllowed = $user->book_allowance;
			// get the item loans for this user and do it old school style
			// for demo purposes of demonstrating mixing styles
			$userloans = $user->loans;
			$userloanCount = 0;
			
			foreach ( $userloans as $loan ) {
				if ($loan->status != 'returned') {
					$userloanCount ++;
				}
			}
			
			// make the view showing user details and loans
			return View::make ( 'Item/return' )->with ( 'user', $user )->with ( 'count', $userloanCount )->with ( 'userloans', $userloans )->with ( 'memberId', $memberId );
		} else {
			$message = "user does not exist";
			return View::make ( 'Item/doReturnMemberId' )->with ( 'message', $message );
		}
	}
	
	// post action to get a book to checkin
	public function doCheckinGetBook() {
	
		// get the form inputs
		$inputs = Input::all ();
		$bookId = $inputs ['bookId'];
		
		// get the loan using the book id
		$loans =Loan::where('book_id', '=', $bookId)->get();
		$bookIdFound = 0;
		$loanFound = null;
		
		if(count($loans)) {
			foreach ( $loans as $loan ) {
				if ($loan->status != 'returned') {
					if ($loan->book_id == $bookId) {
						
						$bookIdFound = $loan->book_id;
						$loanFound = $loan;
					}
				}
			}
			
			if ($bookIdFound != 0) {
				$memberId = $loanFound->user_id;
				// find the particular member
				$user = User::find ( $memberId );
					
				// make sure they are present and are members
				if (count ( $user ) && $user->hasRole ( 'member' )) {
					$booksAllowed = $user->book_allowance;
					// get the item loans for this user and do it old school style
					// for demo purposes of demonstrating mixing styles
					$userloans = $user->loans;
					$userloanCount = 0;
						
					foreach ( $userloans as $loan ) {
						if ($loan->status != 'returned') {
							$userloanCount ++;
						}
					}
						
					// make the view showing user details and loans
					return View::make ( 'Item/return' )->with ( 'user', $user )->with ( 'count', $userloanCount )->with ( 'userloans', $userloans )->with ( 'memberId', $memberId );
				} else {
					$message = "Invalid user.";
					return View::make ( 'Item/doReturnBookId' )->with ( 'message', $message );
				}
			}
			else {
				$message = "Invalid book ID!";
				return View::make ( 'Item/doReturnBookId' )->with ( 'message', $message );
			}
		}
		else {
			$message = "Invalid book ID!";
			return View::make ( 'Item/doReturnBookId' )->with ( 'message', $message );
		}
	}
	
	// post action to get a book to checkin
	public function doBookCheckin() {
	
		// get the form inputs
		$inputs = Input::all ();
		$bookId = $inputs ['bookId'];
		$memberId = $inputs ['memberId'];
		
		// get the loan using the book id
		$loans =Loan::where('book_id', '=', $bookId)->get();
		$bookIdFound = 0;
		$loanFound = null;
	
		if(count($loans)) {
			foreach ( $loans as $loan ) {
				if ($loan->status != 'returned') {
					if ($loan->book_id == $bookId) {
	
						$bookIdFound = $loan->book_id;
						$loanFound = $loan;
					}
				}
			}
				
			if ($bookIdFound != 0) {
				$memberId = $loanFound->user_id;
				// find the particular member
				$user = User::find ( $memberId );
					
				// make sure they are present and are members
				if (count ( $user ) && $user->hasRole ( 'member' )) {
					$booksAllowed = $user->book_allowance;
					// get the item loans for this user and do it old school style
					// for demo purposes of demonstrating mixing styles
					$userloans = $user->loans;
					$userloanCount = 0;
	
					// set loan status as returned
					$loanFound->status = 'returned';
					
					// set the book return date
					$loanFound->updated_at = Carbon::now ();
					//$dueDate = $loanFound->due_date;
					
					// get the number of days overdue
					$daysOut = $loanFound->created_at->diffInDays($loanFound->updated_at);
				
					$globalSetting = GlobalSetting::find(1);
					
					$permissableDays = $globalSetting->permissable_loan_period;
					$daysOverdue = $daysOut - $permissableDays;
					
					// apply fine if overdue
					if ($daysOverdue > 0) {
						$loanRate = $globalSetting->loan_rate;
						$totalFines = $user->outstanding_fine;
						$user->outstanding_fine = $totalFines + ($daysOverdue * $loanRate);
						$user->update();
					}
					
					$loanFound->update();
					// make the book available for loan
					$book = Book::find ( $loanFound->book_id );
					$book->available = true;
					$book->update();
					
					return $this->reloadDoBookCheckin ( $book->id, $user->id );
	
				} else {
					$message = "Invalid user.";
					return View::make ( 'Item/doReturnBookId' )->with ( 'message', $message );
				}
			}
			else {
				$message = "Invalid book ID!";
				return View::make ( 'Item/doReturnBookId' )->with ( 'message', $message );
			}
		}
		else {
			$message = "Invalid book ID!";
			return View::make ( 'Item/doReturnBookId' )->with ( 'message', $message );
		}
	}
	
	// reloads and prepares page after book checkout 
	private function reloadDoBookCheckin($bookId, $memberId) {
		$message = "Successfully checked in book with id " . $bookId;
		// find the particular member
		$user = User::find ( $memberId );
		
		// make sure they are present and are members
		if (count ( $user ) && $user->hasRole ( 'member' )) {
			$booksAllowed = $user->book_allowance;
			
			// get the item loans for this user and do it old school style
			// for demo purposes of demonstrating mixing styles in Laravel
			// and also to show alternatives to the numerous Laravel styles
			// already implemented
			$userloans = $user->loans;
			$userloanCount = 0;
			
			foreach ( $userloans as $loan ) {
				if ($loan->status != 'returned') {
					$userloanCount ++;
				}
			}
			
			// make the view showing user details and loans
			return View::make ( 'Item/return' )->with ( 'user', $user )->with ( 'count', $userloanCount )->with ( 'userloans', $userloans )->with ( 'memberId', $memberId )->with ( 'message', $message );
		} else {
			$message = "user does not exist";
			return View::make ( 'Item/doReturnMemberId' )->with ( 'message', $message );
		}
	}
	
	// shows the initial book lend view to get member
	public function showBookCheckinByMemberId() {
		return View::make ( 'Item/doReturnMemberId' );
	}
	
	// shows the initial book lend view to get member
	public function showBookCheckinByBookId() {
		return View::make ( 'Item/doReturnBookId' );
	}
}