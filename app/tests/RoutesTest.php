<?php
use Carbon\Carbon;
class RouteTest extends TestCase {
	
	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample() {
		$crawler = $this->client->request ( 'GET', '/' );
		
		$this->assertTrue ( $this->client->getResponse ()->isOk () );
	}
	public function testUsernameIsRequired() {
		try { // Create a new User
			$user = $user = new User ();
			/* $user->username = 'Jango'; */
			$user->firstname = 'Jango';
			$user->secondname = 'Foley';
			$user->city = 'slijango';
			$user->address = '12 church lane';
			$user->phone = '089735465';
			$user->outstanding_fine = '0.00';
			$user->email = 'jango.foley@mycit.ie';
			$user->password = Hash::make ( 'foley' );
			$user->status = 'complete';
			$user->type = 'member';
			$user->book_allowance = '4';
			
			$this->assertFalse ( $user->save () );
			
			// Save the errors
			$errors = $user->errors ()->all ();
			
			// There should be 1 error
			$this->assertCount ( 1, $errors );
			
			// The username error should be set
			$this->assertEquals ( $errors [0], "The username field is required." );
		} catch ( Exception $e ) {
		}
	}
	public function testHomePage() {
		$aYearAgo = Carbon::now ()->subYear ();
		$this->call ( 'GET', '/' );
		
		$this->assertViewHas ( 'yearAgo' );
		$this->assertViewHas ( 'yearAgo', $aYearAgo );
		$this->assertViewHas ( 'userloans' );
		$this->assertViewHas ( 'reviews' );
		$this->assertViewHas ( 'numUsers' );
	}
	public function testHomeController2() {
		$crawler = $this->client->request ( 'GET', '/' );
		
		$this->assertTrue ( $this->client->getResponse ()->isOk () );
		$this->assertCount ( 1, $crawler->filter ( 'h7:contains("Book yourself")' ) );
		$this->assertCount ( 1, $crawler->filter ( 'h8:contains("Highest Rated Books")' ) );
		$this->assertCount ( 1, $crawler->filter ( 'h8:contains("Current Most Borrowed Books")' ) );
	}
	public function testAdminReport() {
		$response = $this->call ( 'GET', '/adminReport' );
		
		$view = $response->original;
		
		$this->assertViewHas ( 'finedUserData' );
	}
	public function testRoutes() {
		$response = $this->call ( 'GET', 'bookSearch' );
		$this->assertResponseOk ();
		$this->call ( 'GET', 'approval' );
		$this->assertResponseOk ();
		$this->call ( 'POST', '/updateMembersWithFines' );
		$this->assertResponseOk ();
		$this->call ( 'GET', '/checkinBookByMemberId' );
		$this->call ( 'GET', '/checkinBookByBookId' );
		$this->call ( 'GET', '/membersWithFines' );
	}
}