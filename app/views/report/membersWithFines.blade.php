@extends('layouts.librarian.main')

@section('header')
	LIST OF MEMBERS WITH FINES DUE
@stop
	
	
@section('leftMenu')
@parent
@stop
	
@section('content')
	<section class="booklist">
		<br>{{{ isset($message) ? $message : '' }}}
	@if(count($users) > 0)
	<table> 
		<thead>
		    <tr>
		    <td>Id</td>
		    <td>First name</td>
		    <td>Second name</td>
		    <td>Phone</td>
			<td>Email</td>
			<td>Fine due</td>
			<td>Pay</td>
			<td>Remind</td>
				
			</tr>
		</thead>
 		
 		<tbody>
 		
		@foreach($users as $user)
			{{ Form::open(array('action'=>'FinesController@updateMembersWithFines')) }}
			<tr>
				<td>{{{$user->id}}}</td>
				<td>{{{$user->firstname}}}</a></td>
				<td>{{{$user->secondname}}}</a></td>
				<td>{{{$user->phone}}}</a></td>
				<td>{{{$user->email}}}</td>
				<td>{{{$user->outstanding_fine}}}</td>
				<td><input type="submit" name="payFine" value="Pay all"></td>
				<td><input type="submit" name="sendReminder" value="Send email"></td>
				<label><span></span>{{Form::hidden('user_id', $user->id);}}</label>
				
			{{ Form::close() }}
			</tr>
		@endforeach
		</tbody>
	</table>
	@else
	  <br>There are no members who have fines
	@endif
	</section>
	<br>
	<p>{{ $users->links() }}</p>
	
	<br/>

	@stop