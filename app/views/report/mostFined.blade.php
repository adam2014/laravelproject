@extends('layouts.administrator.main')

@section('header')
  REPORT
@stop

@section('leftMenu')
@parent
@stop

@section('content')
<section class="booklist">
<br>{{{ isset($message) ? $message : '' }}}
<table>
<thead>
   <tr>
       <td>User Id</td>
       <td>Outstanding Fine</td>
<td>Name</td>
<td>Surname</td>
<td>Phone number</td>
<td>E-mail alias</td>
<td>Address</td>
<td>City</td>
<td></td>
</tr>
</thead>

<tbody>
@foreach($finedUserData as $userdata)
<tr>
<td>{{{$userdata->id}}}</td>
   <td>{{{$userdata->outstanding_fine}}}</td>
   <td>{{{$userdata->firstname}}}</td>
   <td>{{{$userdata->secondname}}}</td>
<td>{{{$userdata->phone}}}</a></td>
<td><?php  echo HTML::mailto($userdata->email)?></td>
<td>{{{$userdata->address}}}</td>
<td>{{{$userdata->city}}}</td>
</tr>
@endforeach

</tbody>

</table>

</section>
<br/>
@stop