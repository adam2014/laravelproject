@extends('layouts.member.main')

	@section('header')
		REVIEW - {{{$book->title}}}
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
	
		{{{ isset($message) ? $message : '' }}}
		<br>Title    : {{{$book->title}}}
	    <br>Author   : <a href="{{{URL::to('author')}}}/{{{$book->author->id}}}">{{{$book->author->name}}}</a>
		<br>Isbn     : {{{$book->isbn}}}
		<br>Published: {{{$book->publication_date}}}
		<br>Category : <a href="{{{URL::to('genre')}}}/{{{$book->genre->id}}}">{{{$book->genre->name}}}</a>
		<br>Available: @if ($book->available === "1")
						    Yes
						@else
						    No
						@endif
		
	    <br><br>Review {{{$book->title}}}<br><br>
	   <section class="book_edit_form">
		{{Form::model($review, array('route' => array('review.store'), 'method' => 'post'))}}
		    <h1> Add a new review  
		    <span>&nbsp;</span>
		    </h1>
		    <label><span>Rating (0-5) :</span>{{Form::selectRange('rating',0,5)}}</label>
		    <label><span>Review :</span>{{Form::textarea('comment')}}</label>
			<label><span></span>{{Form::hidden('user_id', 1);}}</label>
								{{Form::hidden('book_id', $book->id);}}
			<label><span>&nbsp;</span>{{Form::submit('Add')}}</label>

    	{{Form::close()}}
    </section> 
	@stop

	@stop
