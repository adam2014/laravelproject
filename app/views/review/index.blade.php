@extends('layouts.member.main') 

@section('header') 
LIST OF BOOKS
PREVIOUSLY LOANED 
@stop
@section('leftMenu') 
@parent 
@stop

@section('content') 
	@if(count($loans) < 1)
		<p>No previously loaned books found</p>
	@elseif(count($loans) == 1)
		<p>1 previously loaned book found</p>
		<br>
		<p>Click on the title to review one of your previously loaned books</p>
		<p>If you wish to see a previous review click on the 'Yes' link under reviewed</p>
	
	@else
	<p>{{{count($loans)}}} books found</p>
	<br>
	<p>Click on a title to review one of your previously loaned books</p>
	<p>If you have already reviewed a book and wish to see a previous review
		click on the 'Yes' link under reviewed</p>
	
	
	@endif
	<br>
	<section class="booklist">
		<br>{{{ isset($message) ? $message : '' }}} <br>
		<table>
			<thead>
				<tr>
					<td>Id</td>
					<td>Title</td>
					<td>Date Borrowed</td>
					<td>Reviewed</td>
				</tr>
			</thead>
	
			<tbody>
	
				@foreach($loans as $loan)
				<tr>
					<td>{{{$loan->book->id}}}</td>
					<td>@if ($loan->reviewed === "0") 
					<a href="{{{URL::to('createReview')}}}/{{{$loan->book->id}}}">{{{$loan->book->title}}}</a>
						@else {{{$loan->book->title}}} @endif
					</td>
	
					<td>{{{$loan->created_at}}}</td>
					<td>@if ($loan->reviewed === "1") 
					<a href="{{{URL::to('review')}}}/{{{$loan->book->id}}}">Yes</a> 
						@else
						No 
						@endif
					</td>
	
				</tr>
				@endforeach
			</tbody>
	
		</table>
	</section>
	<br />
@stop
