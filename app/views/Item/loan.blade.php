@extends('layouts.librarian.main')

	@section('header')
		LEND TO: {{{$user->firstname}}} {{{$user->secondname}}}
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
	<p><b>Member details are:</b></p>
	<p>Username      : {{{$user->username}}}</p>
	<p>Email         : {{{$user->email}}}</p>
	<p>Book Allowance: {{{$user->book_allowance}}}</p>
	<br>
	
	@if($count < 1)
		<p> {{{$user->firstname}}} {{{$user->secondname}}} currently has no books on loan</p> 
		@elseif($count == 1)
			<p> {{{$user->firstname}}} {{{$user->secondname}}} currently has 1 book on loan</p> 
			<p>Book on loan is shown below</p> 
			<section class="booklist">
			
			<table> 
				<thead>
				    <tr>
				        <td>Book Id</td>
						<td>Title</td>
						<td>ISBN</td>
						<td>Author</td>
						<td>Due date</td>
						<td>status</td>
						<td></td>
					</tr>
				</thead>
		 
		 		<tbody>
				@foreach($userloans as $userloan)
				@if($userloan->status != 'returned')
					<tr>
					    <td>{{{$userloan->book->id}}}</td>
						<td>{{{$userloan->book->title}}}</a></td>
						<td>{{{$userloan->book->isbn}}}</td>
						<td>{{{$userloan->book->author->name}}}</a></td>
						<td>{{{$userloan->due_date}}}</td>
						<td>{{{$userloan->status}}}</td>
					</tr>
				@endif
				@endforeach
		
			</tbody>
			</table>
			</section>
			@else
				<p> {{{$user->firstname}}} {{{$user->secondname}}} currently has {{{$count}}} books on loan</p> 
				<p>Books on loan are shown below</p> 
				<br> 
				<section class="booklist">
				<table> 
					<thead>
					    <tr>
					        <td>Book Id</td>
							<td>Title</td>
							<td>ISBN</td>
							<td>Author</td>
							<td>Due date</td>
							<td>status</td>
							<td></td>
						</tr>
					</thead>
			 
			 		<tbody>
					@foreach($userloans as $userloan)
					@if($userloan->status != 'returned')
						<tr>
						    <td>{{{$userloan->book->id}}}</td>
							<td>{{{$userloan->book->title}}}</a></td>
							<td>{{{$userloan->book->isbn}}}</td>
							<td>{{{$userloan->book->author->name}}}</a></td>
							<td>{{{$userloan->due_date}}}</td>
							<td>{{{$userloan->status}}}</td>
						</tr>
					@endif
					@endforeach
			
				</tbody>
				</table>
				</section>
				
	@endif
	
	<br/>
	
	<section class="book_edit_form">
		<h2>{{{ isset($message) ? $message : '' }}}</h2>
		@if($user->book_allowance > $count)
	    	{{Form::open(array('url' => 'doCheckoutBook', 'method' => 'post')) }}
		    <span>&nbsp;</span>
		    </h1>
		    <label><span>Book ID:</span>{{Form::text('book_id')}}</label>
		    {{ Form::hidden('memberId', $user->id) }}
			<label><span>&nbsp;</span>{{Form::submit('Checkout book')}}</label>

    		{{Form::close()}}
    	@else
    		<p><b>Book Allowance exceeded. No more books can be lent</b></p>
    	@endif
    </section> 
	
	@stop