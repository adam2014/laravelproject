@extends('layouts.librarian.main')

	@section('header')
		RETURN FROM MEMBER: {{{$user->firstname}}} {{{$user->secondname}}}
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
	<p><b>Member details returning book are:</b></p>
	<p>Username      : {{{$user->username}}}</p>
	<p>Email         : {{{$user->email}}}</p>
	<p>Book Allowance: {{{$user->book_allowance}}}</p>
	<br>
	<h2>{{{ isset($message) ? $message : '' }}}</h2>
	@if($count < 1)
		<p> {{{$user->firstname}}} {{{$user->secondname}}} currently has no books on loan</p> 
	@else
	
		<p> {{{$user->firstname}}} {{{$user->secondname}}} currently has {{{$count}}} books on loan</p> 
		<p>Books on loan are shown below</p> 
		<br> 
		<section class="booklist">
		<table> 
			<thead>
			    <tr>
			        <td>Book Id</td>
					<td>Title</td>
					<td>ISBN</td>
					<td>Author</td>
					<td>Due date</td>
					<td>status</td>
					<td></td>
				</tr>
			</thead>
	 
	 		<tbody>
			@foreach($userloans as $userloan)
			@if($userloan->status != 'returned')
				<tr>
				    <td>{{{$userloan->book->id}}}</td>
					<td>{{{$userloan->book->title}}}</a></td>
					<td>{{{$userloan->book->isbn}}}</td>
					<td>{{{$userloan->book->author->name}}}</a></td>
					<td>{{{$userloan->due_date}}}</td>
					<td>{{{$userloan->status}}}</td>
					{{Form::open(array('url' => 'doCheckinBook', 'method' => 'post')) }}
					{{ Form::hidden('bookId', $userloan->book->id) }}
					{{ Form::hidden('memberId', $user->id) }}
					<td><input type="submit" name="Check In" value= "Check In"></td>
					{{Form::close()}}
				</tr>
			@endif
			@endforeach
	
		</tbody>
		</table>
		</section>

	@endif
	@stop