@extends('layout')

@section('header')
	TOWN COUNCIL LIBRARY HOME PAGE
@stop
	
	
@section('leftMenu')
@parent
@stop
	
@section('content')
	<br>
	<br>Welcome to the town council book lending library home page.
	<br>To access functionality please choose a menu item on the left hand side.
	<br>
	<br>Any problems should be directed to admin@leabharlann.com
	<br>Alternatively, by phone at 012 345 678 Ext 9
	<br>
	<br>Thank you
	<br>Administration
	<br/>
@stop