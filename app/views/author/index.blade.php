@extends('layouts.member.main')

@section('header')
	LISTING ALL AUTHORS
@stop
	
	
@section('leftMenu')
@parent
@stop
	
@section('content')
	@if(count($authors) < 1)
		<p>No Authors found</p> 
		@elseif(count($authors) == 1)
			<p>1 author found</p> 
			<br>
			<p>Click on the author name to view details and all books by that author</p> 
			@else
				<p>{{{count($authors)}}} authors found</p> 
				<br>
				<p>Click on the author name to view details and all books by that author</p> 
	@endif
	<br>
	<section class="booklist">
	<table> 
		<thead>
		    <tr>
				<td>Name</td>
				<td>Nationality</td>
			</tr>
		</thead>
 
 		<tbody>
		@foreach($authors as $author)
			<tr>
				<td><a href="{{{URL::to('author')}}}/{{{$author->id}}}">{{{$author->name}}}</a></td>
				<td>{{{$author->nationality}}}</td>
			</tr>
		@endforeach
		</tbody>
		
	</table>
	</section>
	<br/>
	@stop