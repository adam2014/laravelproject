@extends('layouts.member.main')

	@section('header')
		Edit {{{$genre->name}}}
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
	<section class="book_edit_form">
		{{Form::model($genre, array('route' => array('genre.update', $genre->id), 'method' => 'put'))}}
		    <h1> {{{$genre->name}}}  
		    <span>{{Form::label('id', $genre>id)}}</span>
		    <span>&nbsp;</span>
		    </h1>
		    
		    <label><span>Name :</span>{{Form::text('name')}}</label>
		    <label><span>Description :</span>{{Form::text('description')}}</label>
			<label><span>&nbsp;</span>{{Form::submit('Update')}}</label>

    	{{Form::close()}}
    </section> 
	@stop
	