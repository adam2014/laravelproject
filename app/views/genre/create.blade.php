@extends('layouts.member.main')

	@section('header')
		Add a genre 
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
	<section class="book_edit_form">
		{{Form::model($genre, array('route' => array('genre.store'), 'method' => 'post'))}}
		    <h1> Please complete fields below to add a new genre 
		    <span>&nbsp;</span>
		    </h1>
		    
		    <label><span>Name :</span>{{Form::text('name')}}</label>
		    <label><span>Description :</span>{{Form::text('description')}}</label>
			<label><span>&nbsp;</span>{{Form::submit('Add')}}</label>

    	{{Form::close()}}
    </section> 
	@stop
	