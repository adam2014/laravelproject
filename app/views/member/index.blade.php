@extends('layouts.librarian.main')

@section('header')
	LIST OF MEMBERS
@stop
	
	
@section('leftMenu')
@parent
@stop
	
@section('content')
	<section class="booklist">
		<br>{{{ isset($message) ? $message : '' }}}
	@if($numberOfMembers > 0)
	<table> 
		<thead>
		    <tr>
		    <td>First name</td>
		    <td>Second name</td>
		    <td>Address</td>
		    <td>City</td>
		    <td>Phone No</td>
	        <td>Id</td>
			<td>Username</td>
			<td>Email</td>
				
			</tr>
		</thead>
 		
 		<tbody>
 		
		@foreach($users as $user)
			@if($user->hasRole('member'))
				<tr>
					<td>{{{$user->firstname}}}</a></td>
					<td>{{{$user->secondname}}}</a></td>
					<td>{{{$user->address}}}</a></td>
					<td>{{{$user->city}}}</a></td>
					<td>{{{$user->phone}}}</a></td>
				    <td>{{{$user->id}}}</td>
					<td>{{{$user->username}}}</a></td>
					<td>{{{$user->email}}}</td>
				</tr>
			@endif
		@endforeach
		</tbody>
	</table>
	<p>{{ $users->links() }}</p>
	@else
	  <br>No members yet
	@endif
	</section>
	<br/>
@stop