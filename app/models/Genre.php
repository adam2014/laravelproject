<?php

// genre model
class Genre extends Eloquent {
	
	// A genre will have many books
	public function books() {
		return $this->hasMany ( 'Book' );
	}
}